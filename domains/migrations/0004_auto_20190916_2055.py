# Generated by Django 2.2.5 on 2019-09-16 17:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('domains', '0003_auto_20190916_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='ban_date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='domain',
            name='new_domain',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='domains.Domain'),
        ),
        migrations.AlterField(
            model_name='domain',
            name='state',
            field=models.IntegerField(choices=[(1, 'куплен'), (2, 'настроен'), (3, 'закрыт'), (4, 'открыт'), (5, 'забанен')]),
        ),
        migrations.AlterField(
            model_name='domain',
            name='use_date',
            field=models.DateField(null=True),
        ),
    ]
