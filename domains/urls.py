from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.DomainListView.as_view(), name='domains'),
    path('search/', views.DomainSearchView.as_view(), name='search'),
    re_path(r'^domain/(?P<pk>\d+)$', views.DomainDetailView.as_view(), name='domain-detail'),
    re_path(r'^domain/(?P<pk>[-\w]+)/ban/$', views.DomainBanView.as_view(), name='domain-ban'),
    re_path(r'^create/$', views.DomainCreate.as_view(), name='domain-create'),
    re_path(r'^create-multiple/$', views.DomainCreateMultiple.as_view(), name='domain-create-multiple'),
    re_path(r'^domain/(?P<pk>[-\w]+)/update/$', views.DomainUpdate.as_view(), name='domain-update'),
    re_path(r'^domain/(?P<pk>[-\w]+)/delete/$', views.DomainDelete.as_view(), name='domain-delete'),
]
