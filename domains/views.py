from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, get_object_or_404, reverse
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.views import generic, View
from .models import Domain
from django.core.paginator import Paginator
from .forms import DomainBanForm, DomainCreateMultipleForm, DomainCheckForm, DomainCreateForm, DomainChangeForm
import datetime


def domains_ready_exclude_pk(pk):
    return Domain.objects.filter(state__in=[2, 3]).exclude(pk=pk)


class DomainListView(PermissionRequiredMixin, View):
    permission_required = ('domains.can_change_domain')

    def get(self, request):
        query = self.request.GET.get('q')
        if query:
            domains = Domain.objects.filter(
                name__contains=query).order_by('name')
        else:
            domains = Domain.objects.all().order_by('name')
        paginator = Paginator(domains, 15)
        page = request.GET.get('page')
        dom = paginator.get_page(page)
        return render(request, 'domains/domain_list.html', {'domain_list': dom})


class DomainDetailView(LoginRequiredMixin, generic.DetailView):
    model = Domain


class DomainBanView(PermissionRequiredMixin, View):
    permission_required = ('domains.ban')

    def get(self, request, pk):
        domain = get_object_or_404(Domain, pk=pk)
        form = DomainBanForm(initial={'state': 5,
                                      'ban_date': datetime.date.today()})
        form.fields['new_domain'].queryset = domains_ready_exclude_pk(pk)
        return render(request, 'domains/domain_ban.html', {'form': form, 'domain': domain})

    def post(self, request, pk):
        domain = get_object_or_404(Domain, pk=pk)
        data = request.POST.copy()
        data['state'] = 5
        form = DomainBanForm(data)

        if form.is_valid():
            domain.state = form.cleaned_data['state']
            domain.ban_date = form.cleaned_data['ban_date']
            domain.new_domain = form.cleaned_data['new_domain']

            domain.new_domain.use_date = datetime.date.today()
            domain.new_domain.state = 4
            domain.new_domain.save()

            domain.save()
            print(domain, domain.ban_date)
            return HttpResponseRedirect(reverse('index'))
        return render(request, 'domains/domain_ban.html', {'form': form, 'domain': domain})


class DomainCreate(LoginRequiredMixin, View):
    def get(self, request):
        form = DomainCreateForm()
        return render(request, 'domains/domain_form.html', {'form': form})

    def post(self, request):
        form = DomainCreateForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            state = form.cleaned_data.get('state')
            use_date = form.cleaned_data.get('use_date')
            ban_date = form.cleaned_data.get('ban_date')
            new_domain = form.cleaned_data.get('new_domain')

            Domain.objects.create(name=name,
                                  state=state,
                                  use_date=use_date,
                                  ban_date=ban_date,
                                  new_domain=new_domain)
            return HttpResponseRedirect(reverse('domains'))
        return render(request, 'domains/domain_form.html', {'form': form})


class DomainUpdate(LoginRequiredMixin, View):
    def get(self, request, pk):
        domain = get_object_or_404(Domain, pk=pk)
        form = DomainChangeForm(instance=domain)
        return render(request, 'domains/domain_form.html', {'form': form, 'domain': domain})

    def post(self, request, pk):
        form = DomainChangeForm(request.POST)
        if form.is_valid():
            domain = get_object_or_404(Domain, pk=pk)
            domain.state = form.cleaned_data.get('state')
            domain.use_date = form.cleaned_data.get('use_date')
            domain.ban_date = form.cleaned_data.get('ban_date')
            domain.new_domain = form.cleaned_data.get('new_domain')
            domain.save()
            return HttpResponseRedirect(reverse('domains'))
        return render(request, 'domains/domain_form.html', {'form': form})


class DomainDelete(LoginRequiredMixin, generic.DeleteView):
    model = Domain
    success_url = reverse_lazy('domains')


class DomainCreateMultiple(LoginRequiredMixin, View):
    def get(self, request):
        form = DomainCreateMultipleForm()
        return render(request, 'domains/domain_form.html', {'form': form})

    def post(self, request):
        form = DomainCreateMultipleForm(request.POST)

        if form.is_valid():
            new_domains = []
            names = form.cleaned_data['domains']
            use_date = form.cleaned_data['use_date']
            state = form.cleaned_data['state']
            for name in names:
                new_domains.append(
                    Domain(name=name, state=state, use_date=use_date))
            Domain.objects.bulk_create(new_domains)

            return HttpResponseRedirect(reverse('domains'))
        return render(request, 'domains/domain_form.html', {'form': form})


class DomainSearchView(LoginRequiredMixin, View):
    def get(self, request):
        query = self.request.GET.get('q')
        context = {}
        if query:
            try:
                domain = Domain.objects.get(name=query)
                return HttpResponseRedirect(domain.get_absolute_url())
            except Domain.DoesNotExist:
                context = {'error': '{} not found'.format(query)}
        print(context)
        return render(request, 'search.html', context)
