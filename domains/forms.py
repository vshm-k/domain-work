import collections
from django import forms
from .models import Domain
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
import datetime

STATES_WITHOUT_BANNED = ((1, 'куплен'),
                         (2, 'настроен'),
                         (3, 'закрыт'),
                         (4, 'открыт'))


class DateInput(forms.DateInput):
    input_type = 'date'


class DomainCheckForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = '__all__'

    def clean_name(self):
        name = self.cleaned_data.get('name')
        validate = URLValidator()
        try:
            validate('http://' + name)
        except ValidationError:
            raise ValidationError('Invalid domain name: {}'.format(name))
        domain_exist = Domain.objects.filter(name=name)
        if domain_exist:
            raise ValidationError(
                'domain name must be unique. {} already exist'.format(name))
        return name

    def clean_use_date(self):
        state = self.cleaned_data.get('state')
        use_date = self.cleaned_data['use_date']
        choices = dict(self.fields['state'].choices)
        if state in [1, 2, 3] and use_date:
            raise ValidationError(
                "Can't set use date for domain in state {}".format(choices[state]))
        if state in [4, 5] and not use_date:
            raise ValidationError(
                "You must set use date for domain in state {}".format(choices[state]))
        return use_date

    def clean_ban_date(self):
        state = self.cleaned_data.get('state')
        print(state)
        ban_date = self.cleaned_data.get('ban_date')
        choices = dict(self.fields['state'].choices)
        if state != 5 and ban_date:
            raise ValidationError(
                "Can't set ban date for domain in state {}".format(choices[state]))
        if state == 5 and not ban_date:
            raise ValidationError(
                "You must set ban date for domain in state {}".format(choices[state]))
        return ban_date

    def clean_new_domain(self):
        state = self.cleaned_data.get('state')
        new_domain = self.cleaned_data.get('new_domain')
        choices = dict(self.fields['state'].choices)
        if state in [1, 2, 3, 4] and new_domain:
            raise ValidationError(
                "Can't set new domain for domain in state {}".format(choices[state]))
        if state == 5 and not new_domain:
            raise ValidationError(
                "You must set new domain for domain in state {}".format(choices[state]))
        return new_domain


class DomainCreateMultipleForm(DomainCheckForm):
    domains = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))
    #state = forms.ChoiceField(choices=STATES_WITHOUT_BANNED, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Domain
        fields = ['domains', 'state', 'use_date']

        widgets = {
            #'domains': forms.Textarea(attrs={'class': 'form-control'}),
            'state': forms.Select(attrs={'class': 'form-control'}),
            'use_date': DateInput(attrs={'class': 'form-control'})
        }

    def clean_domains(self):
        domains = self.cleaned_data.get('domains', '').split()
        validate = URLValidator()
        duplicates = [item for item, count in collections.Counter(
            domains).items() if count > 1]
        if duplicates:
            raise ValidationError(
                'domain name must be unique. {} duplicated'.format(' '.join(duplicates)))
        for d in domains:
            try:
                validate('http://' + d)
            except ValidationError:
                raise ValidationError('Invalid domain name: {}'.format(d))
            domain_exist = Domain.objects.filter(name=d)
            if domain_exist:
                raise ValidationError(
                    'domain name must be unique. {} already exist'.format(d))
        return domains


class DomainCreateForm(DomainCheckForm):
    class Meta:
        model = Domain
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.Select(attrs={'class': 'form-control'}),
            'use_date': DateInput(attrs={'class': 'form-control'}),
            'ban_date': DateInput(attrs={'class': 'form-control'}),
            'new_domain': forms.Select(attrs={'class': 'form-control'})
        }


class DomainChangeForm(DomainCheckForm):
    class Meta:
        model = Domain
        exclude = ('name',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.Select(attrs={'class': 'form-control'}),
            'use_date': DateInput(attrs={'class': 'form-control'}),
            'ban_date': DateInput(attrs={'class': 'form-control'}),
            'new_domain': forms.Select(attrs={'class': 'form-control'})
        }


class DomainBanForm(DomainChangeForm):
    class Meta:
        model = Domain
        fields = ('state', 'ban_date', 'new_domain')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.Select(attrs={'class': 'form-control', 'disabled': True}),
            'use_date': DateInput(attrs={'class': 'form-control'}),
            'ban_date': DateInput(attrs={'class': 'form-control'}),
            'new_domain': forms.Select(attrs={'class': 'form-control'})
        }
