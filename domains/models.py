from django.db import models
from django.shortcuts import reverse


# Create your models here.


class Domain(models.Model):
    name = models.CharField(max_length=50, unique=True)
    STATES = ((1, 'куплен'),
              (2, 'настроен'),
              (3, 'закрыт'),
              (4, 'открыт'),
              (5, 'забанен'))
    state = models.IntegerField(choices=STATES)
    use_date = models.DateField(blank=True, null=True)
    ban_date = models.DateField(blank=True, null=True)

    new_domain = models.OneToOneField(
        'self', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        permissions = (('ban', 'Ban domain'),)

    def get_absolute_url(self):
        return reverse('domain-detail', args=[str(self.id)])

    def __str__(self):
        return self.name
