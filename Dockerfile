# start from an official image
FROM python:3.7.4-alpine

# arbitrary location choice: you can change the directory
RUN mkdir -p /opt/services/djangoapp/src
WORKDIR /opt/services/djangoapp/src

# install our two dependencies
RUN pip install gunicorn django

# copy our project code
COPY . /opt/services/djangoapp/src

# expose the port 9000
EXPOSE 9000

# define the default command to run when starting the container
#CMD ["gunicorn", "--chdir", "app", "--bind", ":8000", "app.wsgi:application"]
