from django.shortcuts import render, redirect

# Create your views here.


def index(request):
    user = request.user
    if user.is_authenticated and user.has_perm('domains.can_change_domain'):
        return redirect('domains')
    if user.is_authenticated:
        return redirect('search')
    else:
        return redirect('login')
