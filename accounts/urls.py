from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from .forms import LoginForm
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', auth_views.LoginView.as_view(authentication_form=LoginForm), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    #path('', RedirectView.as_view(url='domains'))
]
# path('login/', views.LoginView.as_view(authentication_form=LoginForm), name='login'),
